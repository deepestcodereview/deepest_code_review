import torch.nn as nn
from transformers import *

from util.lstm_beam_search import *


class ReviewToCodeModelLSTM(nn.Module):
    def __init__(self, feature_dim, code_characters_size, startI, character_vec_dim=128, output_attentions=False,
                 hidden_dim=256, num_layers=1, should_tune_bert=False, device=None,
                 review_pretrained_weights='bert-base-uncased'):
        super(ReviewToCodeModelLSTM, self).__init__()

        self.device = device if device else torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.startI = startI
        self.code_characters_size = code_characters_size
        self.output_attentions = output_attentions

        self.body_model = BertModel.from_pretrained(review_pretrained_weights).to(self.device)
        if not should_tune_bert:
            for param in self.body_model.parameters():
                param.requires_grad = False

        self.embed = nn.Embedding(self.code_characters_size, character_vec_dim)
        self.affine_proj = nn.Linear(feature_dim, hidden_dim)
        self.lstm = nn.LSTM(character_vec_dim, hidden_dim, num_layers, batch_first=True)
        self.affine_char = nn.Linear(hidden_dim, self.code_characters_size)

        self.loss_func = nn.CrossEntropyLoss()

    def forward(self, X_encoded, X_mask, y_input, y_target, y_target_mask):

        N = X_encoded.shape[0]
        intermediate_encoded, _ = self.body_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )

        # padded value should not contribute to encoding
        # we should not pay attention to them
        intermediate_encoded[np.logical_not(X_mask.astype(bool)), :] = 0.0

        h0 = self.affine_proj(intermediate_encoded.view(N, -1)).unsqueeze(0)

        embedding_out = self.embed(torch.from_numpy(y_input).to(self.device))
        rnn_h, _ = self.lstm(embedding_out, (h0, torch.zeros(h0.shape).to(self.device)))

        score_out = self.affine_char(rnn_h)

        mask = y_target_mask.reshape(-1)
        loss = self.loss_func(
            score_out.view(-1, self.code_characters_size)[mask, :],
            (torch.from_numpy(y_target).to(self.device)).view(-1)[mask],
        )

        return loss

    def sample(self, X_encoded, X_mask, max_length=200, beam_size=1, topK=1):
        self.eval()
        N = X_encoded.shape[0]

        intermediate_encoded, _ = self.body_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )

        # padded value should not contribute to encoding
        # we should not pay attention to them
        intermediate_encoded[np.logical_not(X_mask.astype(bool)), :] = 0.0

        prev_h = self.affine_proj(intermediate_encoded.view(N, -1))
        prev_h = prev_h.unsqueeze(0)
        prev_c = torch.zeros(prev_h.shape).to(self.device)
        states = (prev_h, prev_c)

        return lstm_beam_search(N=N, states=states, startI=self.startI, beam_size=beam_size, topK=topK,
                                max_length=max_length, device=self.device, embed_layer=self.embed, lstm_layer=self.lstm,
                                affine_layer=self.affine_char, output_attentions=self.output_attentions)
