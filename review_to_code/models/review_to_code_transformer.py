import torch.nn as nn
from transformers import *

from util.transformer_beam_search import *


class ReviewToCodeModelTransformer(nn.Module):
    def __init__(self, code_characters_size, startI, padI, should_tune_bert=False, output_attentions=False,
                 num_attention_heads=3, num_hidden_layers=3, intermediate_size=400, device=None,
                 review_pretrained_weights='bert-base-uncased'):
        super(ReviewToCodeModelTransformer, self).__init__()

        self.device = device if device else torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.startI = startI
        self.padI = padI

        self.output_attentions = output_attentions

        self.body_model = BertModel.from_pretrained(review_pretrained_weights, output_attentions=output_attentions).to(
            self.device)
        if not should_tune_bert:
            for param in self.body_model.parameters():
                param.requires_grad = False

        self.code_characters_size = code_characters_size

        self.code_model = BertModel(BertConfig(
            vocab_size=self.code_characters_size, hidden_size=768, num_hidden_layers=num_hidden_layers,
            num_attention_heads=num_attention_heads,
            intermediate_size=intermediate_size, is_decoder=True, output_attentions=output_attentions,
        ))
        self.num_attention_heads = num_attention_heads
        self.num_hidden_layers = num_hidden_layers

        self.loss_func = nn.CrossEntropyLoss()

    def forward(self, X_encoded, X_mask, y_input, y_target, y_target_mask):

        N = X_encoded.shape[0]
        intermediate_encoded = self.body_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )[0]

        last_hidden = self.code_model(input_ids=torch.from_numpy(y_input).to(self.device),
                                      attention_mask=torch.from_numpy(y_target_mask).to(self.device),
                                      encoder_hidden_states=intermediate_encoded,
                                      encoder_attention_mask=torch.from_numpy(X_mask).to(self.device))[0]

        score_out = torch.matmul(last_hidden, torch.transpose(self.code_model.get_input_embeddings().weight, 0, 1))

        mask = y_target_mask.reshape(-1)
        loss = self.loss_func(
            score_out.view(-1, self.code_characters_size)[mask, :],
            (torch.from_numpy(y_target).to(self.device)).view(-1)[mask],
        )

        return loss

    def sample(self, X_encoded, X_mask, max_length=200, beam_size=1, topK=1):
        self.eval()
        N = X_encoded.shape[0]

        intermediate_encoded = self.body_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )[0]
        encoder_attention_mask = torch.from_numpy(X_mask).to(self.device)

        return transformer_beam_search(N=N, beam_size=beam_size, topK=topK,
                                       num_attention_heads=self.num_attention_heads,
                                       max_length=max_length, num_hidden_layers=self.num_hidden_layers,
                                       device=self.device,
                                       startI=self.startI, padI=self.padI, decoder_layer=self.code_model,
                                       encoder_hidden_states=intermediate_encoded,
                                       encoder_attention_mask=encoder_attention_mask,
                                       output_attentions=self.output_attentions)
