from agithub.GitHub import GitHub

import sys
import json
import logging

logging.basicConfig()
logger = logging.getLogger()  # The root logger
logger.setLevel(logging.DEBUG)

token = None if len(sys.argv) <= 1 else sys.argv[1]

from_page = 1 if len(sys.argv) <= 2 else int(sys.argv[2])
to_page = 1002 if len(sys.argv) <= 3 else int(sys.argv[3])

save_at = 100
retry = 4


def main():
    client = GitHub(token=token)

    data = []

    page = from_page
    while page <= to_page:
        try_time = 1
        while True:
            if try_time > retry:
                assert False, "Fail to retry"
            # status, d = client.repos.teammates.teammates.pulls.comments.get(per_page=100, page=page)    
            status, d = client.repos.elastic.elasticsearch.pulls.comments.get(per_page=100, page=page)
            if status == 200:
                data.extend(d)
                break
            else:
                print("Retry as status is {}".format(status))
                try_time += 1

        if page % save_at == 0:
            with open("data-{}.json".format(page), 'w') as outfile:
                json.dump(data, outfile)
            data = []

        print("Try for page {} and success (Records: {})".format(page, len(data)))
        page += 1

    if len(data) != 0:
        with open("data-{}.json".format(page), 'w') as outfile:
            json.dump(data, outfile)
        data = []


if __name__ == "__main__":
    main()