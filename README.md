## Code Repo for "When Deep Neural Net meets Code Review"

The report: https://hackmd.io/@DeepestCodeReview/code_review_with_dnn

The repository contains `Git LFS` objects. Please use `Git LFS` to fetch them. 

#### Usage

```base
pip install -r requirements.txt
```

Tested for `python 3.7` on MacOS and Linux with/without GPU.

The notebook contains some sample runs which can be regarded as examples if you want to try different models.

#### Repo Structure

- `code_to_reivew`
    - `models`: Model we defined in `PyTorch` 
    - `saved_data`: Saved data like trained model
    - `code_bert_viz.ipynb`: Visualization for `CodeBert`
    - `code_encoder_viz.ipynb`: Visualization for Transformer Encoder
    - `code_to_review_lstm_viz.ipynb`: Visualization for LSTM Decoder
    - `code_to_review_transformer_viz.ipynb`: Visualization for Transformer Decoder
- `code_to_score`
    - `models`: Model we defined in `PyTorch` 
    - `saved_data`: Saved data like trained model
    - `code_to_score_viz.ipynb`: Visualization for Transformer Encoder
- `exploratory_data_analysis`
    - `data_elasticsearch`: Tabulated data in Github of `elasticsearch`
    - `data_teammates`: Tabulated data in Github of `teammates`
    - `raw_github`
        - `elasticsearch`: Raw data in Github of `elasticsearch`
        - `teammates`: Raw data in Github of `teammates`
        - `fetch_from_github.py`: Script used to fetch review comments
        - `summary.ipynb`: Give a brief summary of data fetched
    - `eda_teammates.ipynb`: Exploratory Data Analysis (EDA) on `teammates` review comments
- `review_to_code`
    - `models`: Model we defined in `PyTorch` 
    - `saved_data`: Saved data like trained model
    - `review_body_bert_viz.ipynb`: Visualization for `BERT` model
    - `review_to_code_lstm_viz.ipynb`: Visualization for LSTM Decoder
    - `review_to_code_transformer_vis.ipynb`: Visualization for Transformer Decoder
- `util`
    - `bertviz`: Dependency on `https://github.com/jessevig/bertviz`
    - `senti_cr`: Dependency on `https://github.com/senticr/SentiCR`
    - `beam_search_node.py`: Beam search node
    - `head_view_dump.py`: Code to dump attention to `json` file
    - `lstm_beam_search.py`: Common code for beam search in LSTM
    - `training_process_recorder.py`: Common code for Training Process Visualization
    - `transformer_beam_search.py`: Common code for beam search in Transformer Decoder
    - `viz.html`: HTML page to view the dumpped `json` file by `head_view_dump.py`
- `viz`: Notebooks used to draw graphs
- `code_to_review.ipynb`: Task 2 main notebook
- `code_to_review_code_bert.ipynb`: Task 2 main notebook with `CodeBert`
- `code_to_score.ipynb`: Task 1 main notebook
- `code_to_score_code_bert.ipynb`: Task 1 main notebook with `CodeBert`
- `git-add.sh`: Utility file to add Git LFS data
- `git-check.sh`: Utility file to check Git LFS data
- `requirements.txt`: Python dependencies
- `review_to_code.ipynb`: Task 3 main notebook

#### About Visualization

For any visualization, it will call `dump_head_view()`. This will generate a `json` file as `util/viz.tmp.json`. You can view it by uploading it to `viz.html`. We also provide some online examples at https://deepestcodereview.gitlab.io/visualization/attention_viz.html. 
  