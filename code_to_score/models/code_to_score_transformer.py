import numpy as np
import torch
import torch.nn as nn
from transformers import *


class CodeToScoreModelTransformer(nn.Module):
    def __init__(self, code_max_length, is_classification, class_weight, code_characters_size=None,
                 code_pretrained_weights=None, should_tune_encoder=False,
                 hidden_size=400, num_hidden_layers=4, num_attention_heads=4, intermediate_size=500,
                 output_attentions=False):
        super(CodeToScoreModelTransformer, self).__init__()

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        if code_pretrained_weights:
            self.code_model = BertModel.from_pretrained(code_pretrained_weights,
                                                        output_attentions=output_attentions).to(self.device)
            hidden_size = 768
            if not should_tune_encoder:
                for param in self.code_model.parameters():
                    param.requires_grad = False
        else:
            assert code_characters_size is not None
            self.code_model = BertModel(BertConfig(
                vocab_size=code_characters_size, hidden_size=hidden_size,
                num_hidden_layers=num_hidden_layers, num_attention_heads=num_attention_heads,
                intermediate_size=intermediate_size, output_attentions=output_attentions
            ))

        self.affine_1 = nn.Linear(hidden_size * code_max_length, hidden_size)
        self.affine_1_activation = nn.ReLU()

        self.is_classification = is_classification
        if is_classification:
            self.affine_2 = nn.Linear(hidden_size, len(class_weight))
            self.loss_func = nn.CrossEntropyLoss(weight=torch.tensor(class_weight).double().to(self.device))
        else:
            self.affine_2 = nn.Linear(hidden_size, 1)
            self.loss_func = nn.L1Loss()

    def forward(self, X_encoded, X_mask, y_target=None):
        N = X_encoded.shape[0]
        intermediate_encoded = self.code_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )[0]

        # padded value should not contribute to encoding
        # we should not pay attention to them
        intermediate_encoded[np.logical_not(X_mask.astype(bool)), :] = 0.0

        score_out = self.affine_2(self.affine_1_activation(self.affine_1(intermediate_encoded.view(N, -1)))).double()

        if y_target is not None:
            return self.loss_func(score_out, torch.tensor(y_target).to(self.device))

        return score_out
