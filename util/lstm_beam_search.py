import heapq

import numpy as np
import torch

from .beam_search_node import BeamSearchNode


def lstm_beam_search(N, states, startI, beam_size, topK, max_length, device, embed_layer, lstm_layer, affine_layer,
                     output_attentions=False):
    content = []

    if beam_size == 1:
        curr_characters = torch.full((N, 1), startI, dtype=torch.int64).to(device)

        for step in range(max_length):
            embedding_out = embed_layer(curr_characters)
            rnn_h, states = lstm_layer(embedding_out, states)
            score = affine_layer(rnn_h.squeeze(1))
            _, predicted = score.max(1)
            content.append(predicted)
            curr_characters = predicted.unsqueeze(1)

        content = np.expand_dims(torch.stack(content, 1).cpu().numpy(), 1)
    else:
        for batch_idx in range(N):

            endNodes = []
            curr_states = (states[0][:, batch_idx, :].unsqueeze(1), states[1][:, batch_idx, :].unsqueeze(1))
            curr_inputs = torch.LongTensor([[startI]]).to(device)
            curr_node = BeamSearchNode(curr_states, None, curr_inputs, 0, 1)
            pq = []

            heapq.heappush(pq, curr_node)

            while True:
                prev_node = heapq.heappop(pq)

                if prev_node.length > max_length:
                    endNodes.append(prev_node)
                    if len(endNodes) >= topK:
                        break
                    continue

                embedding_out = embed_layer(prev_node.inputs)
                curr_rnn_h, curr_states = lstm_layer(embedding_out, prev_node.h)
                score = torch.nn.functional.log_softmax(affine_layer(curr_rnn_h.squeeze(1)), dim=1)
                log_prob, indexes = torch.topk(score, beam_size)

                for new_k in range(beam_size):
                    curr_inputs = indexes[0][new_k].view(1, -1)
                    log_p = log_prob[0][new_k].item()

                    curr_node = BeamSearchNode(curr_states, prev_node, curr_inputs,
                                               prev_node.logP + log_p, prev_node.length + 1)
                    prev_node.h = None  # release memory
                    heapq.heappush(pq, curr_node)

                # clean up the PQ if it is full
                if len(pq) > 100:
                    pq = heapq.nsmallest(100, pq)

            utterances = []
            for curr_node in sorted(endNodes):
                utterance = [curr_node.inputs.item()]
                # back trace
                while curr_node.prevNode is not None:
                    curr_node = curr_node.prevNode
                    # ignore startI
                    if curr_node.inputs.item() != startI:
                        utterance.append(curr_node.inputs.item())

                utterances.append(list(reversed(utterance)))

            content.append(utterances)
        content = np.stack(content, 0)

    # technically, this should be called as Connectivity
    attention = None
    if output_attentions:
        attention = np.zeros((N, topK, 1, max_length, max_length))
        for k in range(topK):
            curr_k = torch.from_numpy(content[:, k, :]).to(device)
            curr_embedding_out = torch.autograd.Variable(embed_layer(curr_k), requires_grad=True)

            for i in range(1, max_length):
                lstm_layer.zero_grad()
                affine_layer.zero_grad()
                rnn_h, _ = lstm_layer(curr_embedding_out, states)
                score = affine_layer(rnn_h.squeeze(1))
                score[:, i, content[:, k, i]].backward(torch.ones(N, 1).to(device), retain_graph=True)
                attention[:, k, 0, i, :i] = curr_embedding_out.grad.detach().norm(dim=2)[:, :i].cpu().numpy()
                curr_embedding_out.grad.zero_()

    return content, [attention]
