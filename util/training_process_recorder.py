import pickle

import matplotlib.pyplot as plt
import numpy as np
import torch
from matplotlib.ticker import MaxNLocator


class TrainingProcess():
    def __init__(self):
        self.training_loss_step = []
        self.training_loss = []

        self.val_loss_step = []
        self.val_loss = []

        self.test_loss = []

        self.epoch_step = []

        self.curr_step = 0

    def step(self):
        self.curr_step = self.curr_step + 1

    def record_train(self, loss):
        assert not torch.is_tensor(loss)
        self.training_loss_step.append(self.curr_step)
        self.training_loss.append(loss)

    def record_val(self, loss):
        assert not torch.is_tensor(loss)
        self.val_loss_step.append(self.curr_step)
        self.val_loss.append(loss)

    def record_epoch(self):
        self.epoch_step.append(self.curr_step)

    def record_test(self, loss):
        self.test_loss.append(loss)

    def visualize(self, title='Visualization', legend=['Training Loss', 'Validation Loss', 'Test Loss'], ylabel="Loss"):
        test_avg = np.average(self.test_loss)

        ax = plt.figure().gca()
        fig = plt.gcf()
        fig.set_size_inches(12, 6)
        plt.suptitle(title)

        plt.plot(self.training_loss_step, self.training_loss)
        plt.scatter(self.training_loss_step, self.training_loss, s=15)
        plt.plot(self.val_loss_step, self.val_loss)
        plt.scatter(self.val_loss_step, self.val_loss, s=15)
        plt.axhline(y=test_avg, linestyle='dotted', c=tuple([0.96, 0.27, 0.27, 0.9]))
        for s in self.epoch_step:
            plt.axvline(x=s, linestyle='dashed', c=tuple([0.73, 0.73, 0.73, 0.5]))

        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

        plt.legend(legend)
        plt.xlabel('Step')
        plt.ylabel(ylabel)

        plt.show()

    def save(self, file_name):
        with open(file_name, 'wb') as f:
            pickle.dump({
                "training_loss_step": self.training_loss_step,
                "training_loss": self.training_loss,
                "val_loss_step": self.val_loss_step,
                "val_loss": self.val_loss,
                "test_loss": self.test_loss,
                "epoch_step": self.epoch_step,
            }, f)

    def load(self, file_name):
        with open(file_name, 'rb') as f:
            data = pickle.load(f)
            self.training_loss_step = data['training_loss_step']
            self.training_loss = data['training_loss']
            self.val_loss_step = data['val_loss_step']
            self.val_loss = data['val_loss']
            self.test_loss = data['test_loss']
            self.epoch_step = data['epoch_step']
