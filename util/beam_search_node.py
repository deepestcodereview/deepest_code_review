class BeamSearchNode(object):
    def __init__(self, hiddenState, previousNode, inputs, logProb, length):
        self.h = hiddenState
        self.prevNode = previousNode
        self.inputs = inputs
        self.logP = logProb
        self.length = length

    def eval(self):
        return self.logP

    def __lt__(self, other):
        if self.logP > other.logP:
            return True
        else:
            return False
