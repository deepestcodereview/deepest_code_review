import json
import numpy as np

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def dump_head_view(attentions, tokens):
    attentions_tmp = []
    for att in attentions:
        attentions_tmp.append(att.detach().numpy())
    with open('../util/viz.tmp.json', 'w') as f:
        json.dump({
            "tokens": tokens,
            "attentions": attentions_tmp
        }, f, cls=NumpyEncoder)
