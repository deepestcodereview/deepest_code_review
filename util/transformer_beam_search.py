import heapq

import numpy as np
import torch

from .beam_search_node import BeamSearchNode


def transformer_beam_search(N, beam_size, topK, num_attention_heads, max_length,
                            num_hidden_layers, device, startI, padI, decoder_layer, encoder_hidden_states,
                            encoder_attention_mask, output_attentions=False):
    attention_list = None
    if output_attentions:
        attention_list = np.array([np.zeros(
            (N, topK, num_attention_heads, max_length + 1, max_length + 1))] * num_hidden_layers)

    if beam_size == 1:
        y_input = torch.tensor([[startI] + [padI] * max_length] * N).to(device)
        for step in range(max_length):
            decoder_output = decoder_layer(input_ids=y_input, encoder_hidden_states=encoder_hidden_states,
                                           encoder_attention_mask=encoder_attention_mask)
            last_hidden = decoder_output[0]
            if output_attentions:
                for idx, att in enumerate(decoder_output[-1]):
                    attention_list[idx, :, 0, :, (step + 1), :] = att[:, :, step, :].detach().cpu().numpy()

            score_out = torch.matmul(last_hidden,
                                     torch.transpose(decoder_layer.get_input_embeddings().weight, 0, 1))
            _, predicted = score_out[:, step, :].max(1)
            y_input[:, (step + 1)] = predicted

        return np.expand_dims(y_input.cpu().numpy(), 1), attention_list
    else:
        content = []

        for batch_idx in range(N):

            curr_result = []
            curr_encoder_encoded = encoder_hidden_states[batch_idx, :, :].unsqueeze(0)
            curr_encoder_attention_mask = encoder_attention_mask[batch_idx, :].unsqueeze(0)
            curr_inputs = torch.LongTensor([[startI] + [padI] * max_length]).to(device)
            curr_node = BeamSearchNode(None, None, curr_inputs, 0, 1)
            pq = []

            heapq.heappush(pq, curr_node)

            while True:
                prev_node = heapq.heappop(pq)

                if prev_node.length > max_length:
                    curr_result.append(np.squeeze(prev_node.inputs.cpu().numpy(), 0))
                    if len(curr_result) >= topK:
                        curr_result = np.stack(curr_result, 0)
                        break
                    continue

                last_hidden = \
                    decoder_layer(input_ids=prev_node.inputs, encoder_hidden_states=curr_encoder_encoded,
                                  encoder_attention_mask=curr_encoder_attention_mask)[0]

                curr_step_idx = prev_node.length - 1
                score_out = torch.matmul(last_hidden,
                                         torch.transpose(decoder_layer.get_input_embeddings().weight, 0, 1))
                curr_step_score = torch.nn.functional.log_softmax(
                    score_out[:, curr_step_idx, :],
                    dim=1,
                )
                log_prob, indexes = torch.topk(curr_step_score, beam_size)

                for new_k in range(beam_size):
                    curr_word_idx = indexes[0][new_k].view(1)
                    log_p = log_prob[0][new_k].item()
                    curr_inputs = prev_node.inputs.clone()
                    curr_inputs[:, (curr_step_idx + 1)] = curr_word_idx

                    curr_node = BeamSearchNode(None, None, curr_inputs,
                                               prev_node.logP + log_p, prev_node.length + 1)
                    heapq.heappush(pq, curr_node)

                # clean up the PQ if it is full
                if len(pq) > 100:
                    pq = heapq.nsmallest(100, pq)

            content.append(curr_result)

        content = np.stack(content, 0)

        if output_attentions:
            for k in range(topK):
                y_input = torch.tensor([[padI] * (max_length + 1)] * N).to(device)
                for i in range(max_length):
                    y_input[:, i] = torch.tensor(content[:, k, i]).to(device)
                    code_model_output = decoder_layer(input_ids=y_input, encoder_hidden_states=encoder_hidden_states,
                                                      encoder_attention_mask=encoder_attention_mask)
                    for idx, att in enumerate(code_model_output[-1]):
                        attention_list[idx, :, k, :, (i + 1), :] = att[:, :, i, :].detach().cpu().numpy()

        return content, attention_list
