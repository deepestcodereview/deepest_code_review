import torch.nn as nn
from transformers import *

from util.transformer_beam_search import *


class CodeToReviewModelTransformer(nn.Module):
    def __init__(self, startI_w, padI_w, review_vocab_size, code_characters_size=None, output_attentions=False,
                 encoder_pretrained_weights=None, should_tune_encoder=False,
                 encoder_hidden_size=384, encoder_num_layers=4, encoder_num_attention_heads=4,
                 encoder_intermediate_size=256,
                 decoder_hidden_size=384, decoder_num_layers=4, decoder_num_attention_heads=4,
                 decoder_intermediate_size=256):
        super(CodeToReviewModelTransformer, self).__init__()

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.review_vocab_size = review_vocab_size
        self.startI_w = startI_w
        self.padI_w = padI_w
        self.output_attentions = output_attentions

        if encoder_pretrained_weights:
            self.code_model = BertModel.from_pretrained(encoder_pretrained_weights,
                                                        output_attentions=self.output_attentions).to(self.device)
            decoder_hidden_size = 768
            if not should_tune_encoder:
                for param in self.code_model.parameters():
                    param.requires_grad = False
        else:
            assert code_characters_size is not None
            self.code_model = BertModel(BertConfig(
                vocab_size=code_characters_size, hidden_size=encoder_hidden_size,
                num_hidden_layers=encoder_num_layers, num_attention_heads=encoder_num_attention_heads,
                intermediate_size=encoder_intermediate_size, output_attentions=output_attentions
            )).to(self.device)

        self.decoder_num_attention_heads = decoder_num_attention_heads
        self.decoder_num_layers = decoder_num_layers

        self.review_model = BertModel(BertConfig(
            vocab_size=self.review_vocab_size, hidden_size=decoder_hidden_size,
            num_hidden_layers=decoder_num_layers, num_attention_heads=decoder_num_attention_heads,
            intermediate_size=decoder_intermediate_size, is_decoder=True, output_attentions=output_attentions
        )).to(self.device)

        self.loss_func = nn.CrossEntropyLoss()

    def forward(self, X_encoded, X_mask, y_input, y_target, y_target_mask):
        N = X_encoded.shape[0]

        intermediate_encoded = self.code_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )[0]

        last_hidden = self.review_model(input_ids=torch.from_numpy(y_input).to(self.device),
                                        attention_mask=torch.from_numpy(y_target_mask).to(self.device),
                                        encoder_hidden_states=intermediate_encoded,
                                        encoder_attention_mask=torch.from_numpy(X_mask).to(self.device))[0]

        score_out = torch.matmul(last_hidden, torch.transpose(self.review_model.get_input_embeddings().weight, 0, 1))

        mask = y_target_mask.reshape(-1)
        loss = self.loss_func(
            score_out.view(-1, self.review_vocab_size)[mask, :],
            (torch.from_numpy(y_target).to(self.device)).view(-1)[mask],
        )

        return loss

    # generator function
    def sample(self, X_encoded, X_mask, max_length=100, beam_size=1, topK=1):
        self.eval()
        N = X_encoded.shape[0]

        intermediate_encoded = self.code_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )[0]
        encoder_attention_mask = torch.from_numpy(X_mask).to(self.device)

        return transformer_beam_search(N=N, beam_size=beam_size, topK=topK,
                                       num_attention_heads=self.decoder_num_attention_heads,
                                       max_length=max_length, num_hidden_layers=self.decoder_num_layers,
                                       device=self.device,
                                       startI=self.startI_w, padI=self.padI_w, decoder_layer=self.review_model,
                                       encoder_hidden_states=intermediate_encoded,
                                       encoder_attention_mask=encoder_attention_mask,
                                       output_attentions=self.output_attentions)
