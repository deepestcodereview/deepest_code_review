import torch.nn as nn
from transformers import *

from util.lstm_beam_search import *


class CodeToReviewModelLSTM(nn.Module):
    def __init__(self, startI_w, code_max_length, review_vocab_size,
                 code_characters_size=None,
                 code_pretrained_weights=None, should_tune_encoder=False,
                 hidden_size=384, num_hidden_layers=4,
                 num_attention_heads=4, intermediate_size=256,
                 word_embed_dim=256, hidden_dim=256, lstm_layers=1, output_attentions=False):
        super(CodeToReviewModelLSTM, self).__init__()

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        self.review_vocab_size = review_vocab_size
        self.output_attentions = output_attentions
        self.startI_w = startI_w

        if code_pretrained_weights:
            self.code_model = BertModel.from_pretrained(code_pretrained_weights,
                                                        output_attentions=self.output_attentions).to(self.device)
            self.affine_proj = nn.Linear(code_max_length * 768, hidden_dim)
            if not should_tune_encoder:
                for param in self.code_model.parameters():
                    param.requires_grad = False
        else:
            assert code_characters_size is not None
            self.code_model = BertModel(BertConfig(
                vocab_size=code_characters_size, hidden_size=hidden_size,
                num_hidden_layers=num_hidden_layers, num_attention_heads=num_attention_heads,
                intermediate_size=intermediate_size, output_attentions=self.output_attentions
            )).to(self.device)

            self.affine_proj = nn.Linear(code_max_length * hidden_size, hidden_dim)

        # embed layer from word tokens to word_vec
        self.embed = nn.Embedding(self.review_vocab_size, word_embed_dim)
        # affine projection from transformer to hidden state of LSTM
        self.lstm = nn.LSTM(word_embed_dim, hidden_dim, lstm_layers, batch_first=True)
        self.affine_word = nn.Linear(hidden_dim, self.review_vocab_size)

        self.loss_func = nn.CrossEntropyLoss()

    def forward(self, X_encoded, X_mask, y_input, y_target, y_target_mask):
        N = X_encoded.shape[0]

        intermediate_encoded = self.code_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )[0]

        # padded value should not contribute to encoding
        # we should not pay attention to them [batch, seq_len, hidden]
        intermediate_encoded[np.logical_not(X_mask.astype(bool)), :] = 0.0

        h0 = self.affine_proj(intermediate_encoded.view(N, -1)).unsqueeze(0)

        embedding_out = self.embed(torch.from_numpy(y_input).to(self.device))
        rnn_h, _ = self.lstm(embedding_out, (h0, torch.zeros(h0.shape).to(self.device)))

        score_out = self.affine_word(rnn_h)

        mask = y_target_mask.reshape(-1)
        loss = self.loss_func(
            score_out.view(-1, self.review_vocab_size)[mask, :],
            (torch.from_numpy(y_target).to(self.device)).view(-1)[mask],
        )

        return loss

    # generator function
    def sample(self, X_encoded, X_mask, max_length=200, beam_size=1, topK=1):
        N = X_encoded.shape[0]

        intermediate_encoded = self.code_model(
            torch.from_numpy(X_encoded).to(self.device),
            torch.from_numpy(X_mask).to(self.device)
        )[0]

        # padded value should not contribute to encoding
        # we should not pay attention to them
        intermediate_encoded[np.logical_not(X_mask.astype(bool)), :] = 0.0

        prev_h = self.affine_proj(intermediate_encoded.view(N, -1))
        prev_h = prev_h.unsqueeze(0)
        prev_c = torch.zeros(prev_h.shape).to(self.device)
        states = (prev_h, prev_c)

        return lstm_beam_search(N=N, states=states, startI=self.startI_w, beam_size=beam_size, topK=topK,
                                max_length=max_length, device=self.device, embed_layer=self.embed, lstm_layer=self.lstm,
                                affine_layer=self.affine_word, output_attentions=self.output_attentions)
